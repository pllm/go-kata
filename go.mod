module gitlab.com/pllm/go-kata

go 1.18

require gitlab.com/pllm/sequen v0.0.0-20230412201111-ea47ac672c3a

require (
	github.com/brianvoe/gofakeit/v6 v6.21.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
)
