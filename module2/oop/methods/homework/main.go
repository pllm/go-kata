package main

import "fmt"

type calc struct {
	a, b   float64
	result float64
}

func NewCalc() *calc { // конструктор калькулятора
	return &calc{}
}

func (c *calc) SetA(a float64) *calc {
	c.a = a
	return c
}

func (c *calc) SetB(b float64) *calc {
	c.b = b

	return c
}

func (c *calc) Do(operation func(a, b float64) float64) *calc {
	c.result = operation(c.a, c.b)

	return c
}

func (c calc) Result() float64 {
	return c.result
}

func multiply(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a * b
}

func divide(a, b float64) float64 {
	var res float64
	if b == 0 {
		fmt.Println("Err: , dividing by zero")
	} else {
		res = a / b
	}
	return res
}

func sum(a, b float64) float64 {
	return a + b
}

func average(a, b float64) float64 {
	return (a + b) / 2
}
func main() {
	calc := NewCalc()
	resMult := calc.SetA(10).SetB(34).Do(multiply).Result()
	resDiv := calc.SetA(10).SetB(0).Do(divide).Result()
	resSum := calc.SetA(5).SetB(10).Do(sum).Result()
	resAver := calc.SetA(10).SetB(2).Do(average).Result()

	fmt.Println(resMult)
	fmt.Println(resDiv)
	fmt.Println(resSum)
	fmt.Println(resAver)
	fmt.Println(calc.result)
	res2 := calc.Result()
	fmt.Println(res2 == resAver)
}
