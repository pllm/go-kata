package main

import (
	"errors"
	"fmt"
	"testing"
)

func TestMultiply(t *testing.T) {
	testTable := []struct {
		testName       string
		expectedResult float64
		expectedError  error
		a, b           float64
	}{
		{
			testName:       "a = 5 ; b = 2",
			expectedResult: 10,
			expectedError:  nil,
			a:              5, b: 2,
		},
		{
			testName:       "a = 1; b = 7",
			expectedResult: 7,
			expectedError:  nil,
			a:              1, b: 7,
		},
		{
			testName:       "a = 0; b = 10",
			expectedResult: 0,
			expectedError:  nil,
			a:              0, b: 10,
		},
		{
			testName:       "a = -5; b = 10",
			expectedResult: -15,
			expectedError:  nil,
			a:              -5, b: 10,
		},
		{
			testName:       "a = 7; b = 3.3",
			expectedResult: 23.1,
			expectedError:  nil,
			a:              7, b: 3.3,
		},
		{
			testName:       "a = 4.5; b = 7.2",
			expectedResult: 32.4,
			expectedError:  nil,
			a:              4.5, b: 7.2,
		},
		{
			testName:       "a = 0; b = -11.4",
			expectedResult: 11.4,
			expectedError:  nil,
			a:              0, b: -11.4,
		},
		{
			testName:       "a = 0; b = 0",
			expectedResult: 0,
			expectedError:  nil,
			a:              0, b: 0,
		},
	}
	for _, tt := range testTable {
		result := multiply(tt.a, tt.b)
		t.Run(tt.testName, func(t *testing.T) {
			t.Log(tt.testName)

			if result == tt.expectedResult && tt.expectedError != nil {
				t.Error("Test Failed, unexpeccted err", tt.expectedError)
			} else if result != tt.expectedResult && tt.expectedError != nil {
				t.Error("Test Failed")
			}
		})
	}
}

func TestDivide(t *testing.T) {
	testTable := []struct {
		testName       string
		a              float64
		b              float64
		expectedError  error
		expectedResult float64
	}{
		{
			testName:       "a = 10; b = 2",
			a:              10,
			b:              2,
			expectedError:  nil,
			expectedResult: 5,
		},
		{
			testName:       "a = 2; b = 10",
			a:              2,
			b:              10,
			expectedError:  nil,
			expectedResult: 0.2,
		},
		{
			testName:       "a = 10; b = -5",
			a:              10,
			b:              -5,
			expectedError:  nil,
			expectedResult: -2,
		},
		{
			testName:       "a = 10.6; b = -2",
			a:              10.6,
			b:              -2,
			expectedError:  nil,
			expectedResult: -5.3,
		},
		{
			testName:       "a = 10.8; b = 5.2",
			a:              10.8,
			b:              5.2,
			expectedError:  nil,
			expectedResult: 2.4,
		},
		{
			testName:       "a = 14; b = 7",
			a:              14,
			b:              7,
			expectedError:  nil,
			expectedResult: 2,
		},
		{
			testName:       "a = 0; b = 10",
			a:              0,
			b:              10,
			expectedError:  nil,
			expectedResult: 0,
		},
		{
			testName:       "a = 10; b = 0",
			a:              10,
			b:              0,
			expectedError:  errors.New("Err: , dividing by zero"),
			expectedResult: 0,
		},
	}
	for _, tt := range testTable {
		result := divide(tt.a, tt.b)
		t.Run(tt.testName, func(t *testing.T) {
			t.Log(tt.testName)

			if result == tt.expectedResult && tt.expectedError != nil {
				t.Error("Test Failed, err: ", tt.expectedError)
			} else if result != tt.expectedResult && tt.expectedError != nil {
				t.Error("Test Failed")
			}
		})
	}
}

func TestSum(t *testing.T) {
	type testStruct struct {
		a, b, res float64
	}
	testTable := []testStruct{
		{1, 2, 3},
		{5, 5, 10},
		{5, 0, 5},
		{-5, -5, -10},
		{0, 0, 0},
		{10.5, 7.2, 17.7},
		{0, 2, 2},
		{10, 2.5, 12.5},
	}
	for _, tt := range testTable {
		expectedResult := tt.res
		result := sum(tt.a, tt.b)
		if result != expectedResult {
			fmt.Println("TestFailed: result", result)
		} else {
			fmt.Println("ok")
		}
	}
}

func TestAverage(t *testing.T) {
	type testStruct struct {
		a, b, res float64
	}
	testTable := []testStruct{
		{5, 5, 5},
		{0, 5, 2.5},
		{-10, 0, -5},
		{5, -10, -2.5},
		{-10, -10, -10},
		{-5, -1, -3},
		{0, 1, 0.5},
		{-5.6, -2, -3.8},
	}
	for _, tt := range testTable {
		expectedResult := tt.res
		result := average(tt.a, tt.b)
		if result != expectedResult {
			fmt.Println("TestFailed: result", result)
		} else {
			fmt.Println("ok")
		}
	}
}
