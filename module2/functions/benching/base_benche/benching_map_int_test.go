package base_benche

import "testing"

func insertXIntMap(x int, b *testing.B) {
	// Инициализируем Map и вставляем X элементов
	testmap := make(map[int]int, 0)
	// Сбрасываем таймер после инициализации Map
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// Вставляем значение I в ключ I.
		testmap[i] = i
	}
}
func BenchmarkInsertIntMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntMap(1000, b)
	}
}

func BenchmarkInsertIntMap100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntMap(100, b)
	}
}
