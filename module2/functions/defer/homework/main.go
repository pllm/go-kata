package main

import (
	"errors"
	"fmt"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

// BEGIN (write your solution here)
func main() {
	result, err := ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, {"b": "c"}}})
	if err == errNotEnoughDicts {
		// fmt.Println(result.IsFinished, err)
		fmt.Printf("&MergeDictsJob{IsFinished: %t}, \"%v\"\n", result.IsFinished, err)
	} else if err == errNilDict {
		// fmt.Println(result.IsFinished, result.Dicts, err)
		fmt.Printf("&MergeDictsJob{IsFinished: %t, Dicts: %#v}, \"%v\"\n", result.IsFinished, result.Dicts, err)
	} else if err == nil {
		// fmt.Println(result.IsFinished, result.Merged, err)
		fmt.Printf("&MergeDictsJob{IsFinished: %t, Dicts: []%#v}, \"%v\"\n", result.IsFinished, result.Merged, err)
	}

}

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{}) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	job.IsFinished = true
	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts // main( if err == expectedError...)
	}
	for val := range job.Dicts {
		if job.Dicts[val] == nil {
			return job, errNilDict
		}
	}
	job.Merged = make(map[string]string)
	for val := range job.Dicts {
		for key, value := range job.Dicts[val] {
			job.Merged[key] = value
		}
	}
	return job, nil
}

// END
