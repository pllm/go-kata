package main

import (
	"fmt"
	"unicode"
	"unicode/utf8"
)

func Greet(name string) string {
	for i := 0; i < len(name); i++ {
		r, _ := utf8.DecodeRuneInString(name)
		if unicode.Is(unicode.Cyrillic, r) {
			return fmt.Sprintf("Привет %s, добро пожаловать!", name)
		} else if unicode.Is(unicode.Latin, r) {
			return fmt.Sprintf("Hello %s, you welcome!", name)
		}
	}
	return fmt.Sprintf("Error unknown name: %s", name)
}
