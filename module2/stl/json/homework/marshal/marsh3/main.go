package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	followers int
	Grades    map[string]string
}

type Student struct {
	FirstName, lastName string
	Age                 int
	Profile             Profile
	Languages           []string
}

func main() {

	var john Student

	john = Student{
		FirstName: "John",
		lastName:  "Doe",
		Age:       21,
		Profile: Profile{
			Username:  "johndoe91",
			followers: 1975,
			Grades:    map[string]string{"Math": "A", "Science": "A+"},
		},
		Languages: []string{"English", "French"},
	}

	johnJSON, err := json.MarshalIndent(john, "", "  ")

	fmt.Println(string(johnJSON), err)
}

//type ProfileI interface {
//	Follow()
//}
//
//type Profile struct {
//	Username  string
//	Followers int
//}
//
//func (p *Profile) Follow() {
//	p.Followers++
//}
//
//type Student struct {
//	FirstName, lastName string
//	Age                 int
//	Primary             ProfileI
//	Secondary           ProfileI
//}
//
//func main() {
//
//	john := &Student{
//		FirstName: "John",
//		lastName:  "Doe",
//		Age:       21,
//		Primary: &Profile{
//			Username:  "johndoe91",
//			Followers: 1975,
//		},
//	}
//
//	john.Primary.Follow()
//
//	johnJSON, _ := json.MarshalIndent(john, "", "  ")
//
//	fmt.Println(string(johnJSON))
//}
