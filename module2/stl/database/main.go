package main

import (
	"database/sql"
	"fmt"

	_ "github.com/mattn/go-sqlite3"
)

func main() {
	database, _ := sql.Open("sqlite3", "./gopher.db")
	statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY AUTOINCREMENT, firstname TEXT, lastname TEXT)")
	_, err := statement.Exec()
	if err != nil {
		fmt.Println("error")
	}
	statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
	_, err = statement.Exec("Petr", "Chech")
	if err != nil {
		fmt.Println("error")
	}
	rows, err := database.Query("SELECT id, firstname, lastname FROM people")
	if err != nil {
		fmt.Println("error")
	}
	var id int
	var firstname, lastname string

	for rows.Next() {
		rows.Scan(&id, &firstname, &lastname)
		fmt.Printf("%d: %s %s\n ", id, firstname, lastname)
	}
}
