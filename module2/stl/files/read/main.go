package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {
	dat, err := os.ReadFile("text.txt")
	check(err)
	fmt.Println(string(dat))

	file, err := os.Open("text.txt")
	check(err)

	b1 := make([]byte, 5)
	n1, err := file.Read(b1)
	check(err)

	fmt.Printf("%d bytes: %s\n", n1, string(b1[:n1]))
	defer file.Close()

	o2, err := file.Seek(6, 0)
	check(err)

	b2 := make([]byte, 2)
	n2, err := file.Read(b2)
	check(err)

	fmt.Printf("%d bytes @ %d: \n", n2, o2)
	fmt.Printf("%v\n", string(b2[:n2]))

	o3, err := file.Seek(6, 0)
	check(err)

	b3 := make([]byte, 2)
	n3, err := io.ReadAtLeast(file, b3, 2)
	check(err)
	fmt.Printf("%d bytes @ %d: %s\n", n3, o3, string(b3))

	r4 := bufio.NewReader(file)
	b4, err := r4.Peek(5)
	check(err)
	fmt.Printf("5 bytes: %s\n", string(b4))
}
