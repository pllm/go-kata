package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	file, err := os.Create("text.txt")
	if err != nil {
		fmt.Println("Unable to create file:", err)
	}
	defer file.Close()
	file.WriteString(name)
	fmt.Printf("Hello %s\n", name)

}
