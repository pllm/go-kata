package main

import (
	"bufio"
	"fmt"
	"os"
	"unicode"
	"unicode/utf8"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter some text: ")

	text, _ := reader.ReadString('\n')
	file, err := os.Create("example.txt")
	if err != nil {
		fmt.Println("Unable ti create file:", err)
	}
	defer file.Close()
	file.WriteString(text)

	file2, err := os.Create("example.processed.txt")
	if err != nil {
		fmt.Println("Unable to create file:", err)
	}

	f, err := os.ReadFile("example.txt")
	if err != nil {
		fmt.Println("Err reading file:", err)
	}
	textTrans := TransiltCyrToLat(string(f))

	file2.WriteString(textTrans)

}

func TransiltCyrToLat(data string) string {
	var newdata string
	transMap := map[rune]string{
		'а': "a",
		'б': "b",
		'в': "v",
		'г': "g",
		'д': "d",
		'е': "e",
		'ё': "yo",
		'ж': "zh",
		'з': "z",
		'и': "i",
		'й': "j",
		'к': "k",
		'л': "l",
		'м': "m",
		'н': "n",
		'о': "o",
		'п': "p",
		'р': "r",
		'с': "s",
		'т': "t",
		'у': "u",
		'ф': "f",
		'х': "h",
		'ц': "c",
		'ч': "ch",
		'ш': "sh",
		'щ': "sch",
		'ъ': "'",
		'ы': "y",
		'ь': "",
		'э': "e",
		'ю': "ju",
		'я': "ja",
		'А': "A",
		'Б': "B",
		'В': "V",
		'Г': "G",
		'Д': "D",
		'Е': "E",
		'Ё': "Yo",
		'Ж': "Zh",
		'З': "Z",
		'И': "I",
		'Й': "J",
		'К': "K",
		'Л': "L",
		'М': "M",
		'Н': "N",
		'О': "O",
		'П': "P",
		'Р': "R",
		'С': "S",
		'Т': "T",
		'У': "U",
		'Ф': "F",
		'Х': "H",
		'Ц': "C",
		'Ч': "Ch",
		'Ш': "Sh",
		'Щ': "Sch",
		'Э': "E",
		'Ю': "Ju",
		'Я': "Ja",
	}
	for len(data) > 0 {
		r, size := utf8.DecodeRuneInString(data)
		if unicode.Is(unicode.Cyrillic, r) {
			newdata += transMap[r]
		} else {
			newdata += data[:size]
		}
		data = data[size:]
	}
	return newdata
}
