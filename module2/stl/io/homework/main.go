package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	Buf := new(bytes.Buffer)
	// запишите данные в буфер
	for _, v := range data {
		Buf.WriteString(v)
	}
	// создайте файл
	file, err := os.Create("example.txt")
	if err != nil {
		fmt.Println("Unable to write the file")
	}
	// запишите данные в файл
	defer file.Close()
	_, err = io.Copy(file, Buf)
	if err != nil {
		fmt.Println("error: unable copy to file")
	}
	// прочтите данные в новый буфер
	newBuf, err := os.ReadFile("example.txt")
	if err != nil {
		fmt.Println("error: unable reading file")
	}
	// выведите данные из буфера buffer.String()
	fmt.Println(string(newBuf))
	// у вас все получится!
}
