package main

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"name"`
	Production bool
}

func main() {
	var configFile string
	errOpen := errors.New("error: Unable to open file")
	errRead := errors.New("error: Unable to read file")
	flag.StringVar(&configFile, "conf", "", "file")
	flag.Parse()
	f, err := os.Open("config.json")
	if err != nil {
		fmt.Println(errOpen)
	}
	defer f.Close()

	conf := Config{}
	val, err := os.ReadFile("config.json")
	if err != nil {
		fmt.Println(errRead)
	}
	_ = json.Unmarshal(val, &conf)
	fmt.Printf("Production: %v\nAppName: %v\n", conf.Production, conf.AppName)
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
