package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}
	subUsers := users[2:len(users)]
	copy(subUsers, subUsers)
	editSecondSlice(subUsers)

	fmt.Println(users)
	fmt.Println(subUsers)
}

func editSecondSlice(users []User) {
	for i := range users {
		users[i].Name = "unknown"
	}
}
