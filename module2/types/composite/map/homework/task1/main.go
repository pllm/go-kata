// You can edit this code!
// Click here and start typing.
package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/golang/go/wiki/GoUsers",
			Stars: 110000,
		},
		{
			Name:  "https://github.com/a8m/golang-cheat-sheet",
			Stars: 7500,
		},
		{
			Name:  "https://github.com/quii/learn-go-with-tests",
			Stars: 19400,
		},
		{
			Name:  "https://github.com/golang-standards/project-layout.git",
			Stars: 38700,
		},
		{
			Name:  "https://github.com/nats-io/nats.go.git",
			Stars: 4500,
		},
		{
			Name:  "https://github.com/gorilla/mux.git",
			Stars: 18100,
		},
		{
			Name:  "https://github.com/apache/kafka.git",
			Stars: 24500,
		},
		{
			Name:  "https://github.com/docker/compose.git",
			Stars: 29000,
		},
		{
			Name:  "https://github.com/mongodb/mongo.git",
			Stars: 23500,
		},
		{
			Name:  "https://github.com/git/git.git",
			Stars: 45700,
		},
		{
			Name:  "https://github.com/postgres/postgres.git",
			Stars: 12100,
		},
		{
			Name:  "https://github.com/grpc/grpc-go.git",
			Stars: 17800,
		},
		// сюда впишите ваши остальные 12 структур
	}
	var projectsMap = make(map[string]int)
	// в цикле запишите в map
	for _, v := range projects {
		projectsMap[v.Name] = v.Stars
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for k, v := range projectsMap {
		fmt.Println("key:", k, "value:", v)
	}
}
