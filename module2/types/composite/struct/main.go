package main

import (
	"fmt"
	"unsafe"
)

//User Объявление пользовательского типа (структуры)
type User struct {
	Age      int
	Name     string
	Wallet   Wallet   // Добавили поле Wallet
	Location Location // Добавили поле Location
}
type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}
type Location struct {
	Address string
	City    string
	index   string
}

func main() {
	//Создаем переменную user с нашим типом User
	user := User{
		Age:  13,         // заполняем поля структуры
		Name: "Alexandr", // заполняем поля структуры
	}
	fmt.Println(user)

	wallet := Wallet{
		RUR: 25000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}

	user2 := User{
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			RUR: 14400,
			USD: 8900,
			BTC: 55,
			ETH: 34,
		},
		Location: Location{
			Address: "Нововатутинская 3-я ул, 13, к.2",
			City:    "Москва",
			index:   "108836",
		},
	}
	fmt.Println(wallet)
	fmt.Println("wallet alocates: ", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)
	fmt.Println(user2)
}
