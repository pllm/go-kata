package main

import "fmt"

//User Объявление пользовательского типа (структуры)
type User struct {
	Age    int
	Name   string
	Wallet Wallet // Добавили поле Wallet
}
type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func rangeArray() {
	users := [4]User{
		{
			Age:  8,
			Name: "John",
			Wallet: Wallet{
				RUR: 13,
				USD: 1,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  13,
			Name: "Katie",
			Wallet: Wallet{
				RUR: 500,
				USD: 3,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			Age:  21,
			Name: "Doe",
			Wallet: Wallet{
				RUR: 0,
				USD: 300,
				BTC: 1,
				ETH: 3,
			},
		},
		{
			Age:  34,
			Name: "Arnie",
			Wallet: Wallet{
				RUR: 987342,
				USD: 34,
				BTC: 1,
				ETH: 3,
			},
		},
	}
	// выводим пользователей старше 18 лет
	fmt.Println("Пользователи старше 18 лет:")
	for i := range users {
		if users[i].Age > 18 { // проверяем возраст больше 18
			fmt.Println(users[i]) // выводим в терминал, если условие выолняется
		}
	}
	fmt.Println("Пользователи у которых есть криптовалюта на балансе:")
	for i, user := range users {
		if users[i].Wallet.BTC > 0 || users[i].Wallet.ETH > 0 {
			fmt.Println("user:", user, "index:", i)
		}
	}
	fmt.Println("======= end users non zero crypto balance  ========")
}

func main() {
	//создаем массив
	a := [...]int{34, 55, 89, 144}
	//выыодим массив в терминал
	fmt.Println("original value", a)

	//меняем значение первого элемента
	a[0] = 21
	// выводим массив с изменненым первым элементом
	fmt.Println("changed first element", a)

	//создаем копию массива
	b := a
	//меняем значения массиве а
	a[0] = 233
	fmt.Println("original array", a)
	fmt.Println("modified array", b)

	//выводим пользователей старше 18 лет
	fmt.Println("Пользователи старше 18 лет:")

}
