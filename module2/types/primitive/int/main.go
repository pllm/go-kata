package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeInt()
}

func typeInt() {
	fmt.Println("=== START int value ===")
	var numberUint8 uint8 = 1 << 7
	var min8 = int8(numberUint8)
	numberUint8--
	var max8 = int8(numberUint8)
	fmt.Println("int8 min value:", min8, "int8 max value", max8, unsafe.Sizeof(min8), "bytes")
	var numberUint16 uint16 = 1 << 15
	var min16 = int16(numberUint16)
	numberUint16--
	var max16 = int16(numberUint16)
	fmt.Println("int16 min value:", min16, "int16 max value", max16, unsafe.Sizeof(min16), "bytes")
	var numberUint32 = 1 << 31
	var min32 = int32(numberUint32)
	numberUint32--
	var max32 = int32(numberUint32)
	fmt.Println("int32 min value:", min32, "int32 max value", max32, unsafe.Sizeof(min32), "bytes")
	var numberUint64 uint64 = 1 << 63
	var min64 = int64(numberUint64)
	numberUint64--
	var max64 = int64(numberUint64)
	fmt.Println("int64 min value:", min64, "int64 max value", max64, unsafe.Sizeof(min64), "bytes")
	fmt.Println("=== END int value ===")
}
