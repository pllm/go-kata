package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 11235832134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeBool()
}

func typeBool() {
	var b bool
	fmt.Println("размер в байтах:", unsafe.Sizeof(b), "bytes") // размер в байтах 1

	var u uint8 = 1                  // берем uint8 - 8бит, выставляем значене в 1
	fmt.Println(b)                   // bool - по дефолтное значение false
	b = *(*bool)(unsafe.Pointer(&u)) // берем значение из uint8 проставим в тип  bool
	fmt.Println(b)                   // распечатаем значени true
}
