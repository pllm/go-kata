package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 11235832134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeByte()
}

func typeByte() {
	var b byte = 124
	fmt.Println("размер в байтах:", unsafe.Sizeof(b))
}
