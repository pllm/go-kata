package main

import "fmt"

func main() {
	var numberInt int                // создаем переменную(обьект) типа integer
	numberInt = 3                    // присваиваем значение 3
	var numberFloat float32          //создаем переменную(обьект) типа float32
	numberFloat = float32(numberInt) // присваем в numberFloat переменную numberInt,которую явно преобразовываем во float32

	fmt.Printf("type: %T, value: %v", numberFloat, numberFloat)
}
