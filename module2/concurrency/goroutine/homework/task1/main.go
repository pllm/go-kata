package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	t := time.Now()
	rand.Seed(t.UnixNano())

	parseURl("http://example.com/")
	parseURl("http://youtube.com/")

	fmt.Println("Parsing completed. Time Elapsed: %f seconds\n", time.Since(t).Seconds())
}

func parseURl(url string) {
	for i := 0; i < 5; i++ {
		latency := rand.Intn(500) + 500
		time.Sleep(time.Duration(latency) * time.Millisecond)
		fmt.Printf("Parsing <%s> - Step %d - Latency%d\n", url, i+1, latency)
	}
}
