package main

import "fmt"

func main() {
	// make buf chanel
	message := make(chan string, 2)
	message <- "hello"
	message <- "Go"
	fmt.Println(<-message)
	message <- "!"
	fmt.Println(<-message)
	fmt.Println(<-message)
}
