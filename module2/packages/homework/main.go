package main

import (
	"fmt"

	sequen "gitlab.com/pllm/sequen"
)

func main() {
	myOct := sequen.Octopus{Name: "Zoidberg", Color: "pink"}
	sequen.Hello()
	fmt.Println(myOct.String())

}
